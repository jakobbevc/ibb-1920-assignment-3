from sklearn.metrics import mean_squared_error
import numpy as np
import pandas as pd
from random import randint
#from data.process_data import load_data, normalize_data, train_test_sets
from sklearn.model_selection import KFold
from sklearn.metrics import accuracy_score

import matplotlib.pyplot as plt


#RMSE ERROR FOR TRAIN AND TEST DATA
def classification_accuracy(y_train, y_train_predict, y_test, y_test_predict):
    train_acc = accuracy_score(y_train, y_train_predict) * 100
    test_acc = accuracy_score(y_test, y_test_predict) * 100
    print('Accuracy on train data: {0:.6f} %'.format(train_acc))
    print('Accuracy on test data: {0:.6f} %'.format(test_acc))

    pass


def load_combined_dataset(path_processed):

    print("Loading data...")

    path = path_processed + 'all_datasets_combined.csv'
    df_data = pd.read_csv(path)#, dtype=np.float64)

    true_labels = df_data['labels'].values#.reshape(-1, 1) 
    true_labels = true_labels.astype(int)

    df_data.drop(['labels'], axis=1, inplace=True)
    data = df_data.values

    n_samples, n_features = data.shape

    print("Loaded data of size: ({},{})".format(n_samples, n_features))

    return data, true_labels


def train_test_split(X, y, test_size=0.3):

    train_size = 1 - test_size
    n_samples, n_features = X.shape

    X_train = X[:int(n_samples * train_size), :]
    y_train = y[:int(n_samples * train_size)]

    X_test = X[int(n_samples * train_size):, :]
    y_test = y[int(n_samples * train_size):]

    print("X_train shape is: {}".format(X_train.shape))
    print("y_train shape is: {}".format(y_train.shape))
    print("X_test shape is: {}".format(X_test.shape))
    print("y_test shape is: {}".format(y_test.shape))

    return X_train, y_train, X_test, y_test





































def prepare_data(data_df, enc_labels,  batch_size):
#     X = data_df[features].values
#     y = data_df[labels].values

    X_list = []
    y_list = []
    for idx in range(len(data_df) - batch_size):
        X_batch = data_df[idx:idx+batch_size]
        y_batch = enc_labels[idx:idx+batch_size]
        if len(X_batch) == batch_size:
            X_list.append(X_batch)
            y_list.append(y_batch)

    #return np.array(X_list), np.array(y_list)
    return X_list, y_list
    



def data_loader(batch_size, fp, dropcol, seq_len):
    for f in fp:
        #gc.collect()
        #df=pd.read_csv(f)
        
        
        df = df_alwin_1
        #df=df.replace(np.nan, 0)
        df=df.drop(dropcol,1)
        #df['minute'] = df['minute'].apply(lambda x: min_idx(x))
        row_count, col_count = df.shape
        encoder_input = []
        prev = 0
        for idx, b in enumerate(range(1, row_count)):
            end = prev + batch_size
            window = df.iloc[prev:end]
            prev = end - 1
            w = np.array(window, dtype='float64')

            if w.shape[0] != batch_size:  break
            encoder_input.append(w)
                
            if idx == seq_len:
                w0 = encoder_input
                encoder_input = []
                yield w0



def cross_valid(X_cross, y_cross, model_name):
    results_cross = []
    k_folds = KFold(n_splits=5, shuffle=False)
    for train, test in k_folds.split(X_cross):
        train_XX, test_XX = X_cross[train], X_cross[test]
        train_yy, test_yy = y_cross[train], y_cross[test]

        y_train_predict, y_test_predict = model_name(train_XX, train_yy, 
                                                     test_XX, load_model=False)

        rmse_error = np.sqrt(mean_squared_error(test_yy, y_test_predict))
        results_cross.append(rmse_error)

    mean_rmse = np.round(np.mean(results_cross), 2)
    std_dev = np.round(np.std(results_cross), 2)
    n_mean_rmse = np.round((mean_rmse / 17000) * 100, 4)

    print("Cross validaton results:")
    print("Mean error: " + str(mean_rmse) + " +/- " + str(std_dev) + " W")
    print("Mean error percentage: " + str(n_mean_rmse) + " %")

if __name__ == __name__:
    ''' Test setups for pytorch LSTM '''
    # X, y = prepare_data(df_alwin_1.values, encoded_labels, batch_size)
    # X = torch.tensor(X, dtype=torch.float32)
    # y = torch.tensor(y, dtype=torch.float32)
    # print(X)
    # print(y)

    # print(X.shape)
    # print(y.shape)

    # seq_len = len(X)

    # X = torch.randn(1, 5, num_features)
    # target=np.array([[1,0,0,0,0,0,0],
    #                  [1,0,0,0,0,0,0],
    #                  [1,0,0,0,0,0,0],
    #                  [1,0,0,0,0,0,0],
    #                  [1,0,0,0,0,0,0]])
    # y = torch.tensor(target, dtype=torch.long)
    # print(X)
    # print(y)


    # print(X.dtype)
    # print(y.dtype)

    # print(X.shape)
    # print(y.shape)




    # X = torch.randn(1, 5, num_features)
    # target=np.array([[1,0,0,0,0]])
    # # target = np.array([0])

    # y = torch.tensor(target, dtype=torch.long)
    # y = y.view(1, 5)
    # print(X)
    # print(y)

    # print(X.shape)
    # print(y.shape)





    