import pickle
import warnings
import sys

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import lightgbm as lgb

from time import time
from datetime import datetime

from sklearn.linear_model import LinearRegression
from sklearn.neural_network import MLPClassifier
from sklearn.ensemble import RandomForestRegressor, GradientBoostingRegressor
from sklearn.metrics import mean_squared_error, r2_score, make_scorer
from sklearn.model_selection import GridSearchCV, RandomizedSearchCV, KFold
from sklearn.svm import SVC
from sklearn.metrics import plot_confusion_matrix

from sklearn.model_selection import train_test_split

from sklearn.metrics import accuracy_score

from scipy.optimize import least_squares

# Adds higher directory to python modules path.
sys.path.append("..")
warnings.filterwarnings("ignore")

# LOCAL FILES
from utils.utils import classification_accuracy
from data.process_data import train_test_sets
# from data.process_data import load_data, normalize_data, train_test_sets, load_arso_data, load_new_pv_data, load_data_MIDEM, load_data_FULL
# from models.empirical_models import display_coefficients, fx_RMSE, func_Linear_model, func_NONLINEAR_model
# from data.data_load import load_data


def cross_valid(X_cross, y_cross, model_name):
    results_cross = []
    k_folds = KFold(n_splits=5, shuffle=False)
    for train, test in k_folds.split(X_cross):
        train_XX, test_XX = X_cross[train], X_cross[test]
        train_yy, test_yy = y_cross[train], y_cross[test]

        y_train_predict, y_test_predict = model_name(train_XX, train_yy, 
                                                     test_XX, load_model=False)

        accuracy = accuracy_score(test_yy, y_test_predict) * 100
        results_cross.append(accuracy)

    mean_acc = np.round(np.mean(results_cross), 2)
    std_dev = np.round(np.std(results_cross), 2)

    print("Cross validaton results:")
    print("Mean accuracy: " + str(mean_acc) + " +/- " + str(std_dev) + " W")


def GBM(X_train, y_train, X_test, load_model=False):
    if load_model:
        open_file = open("GMB_parameters.pickle", "rb")
        model = pickle.load(open_file)
    else:
        model = GradientBoostingRegressor(n_estimators=1000, learning_rate=0.05, max_depth=5, random_state=0, loss='ls')
        model.fit(X_train, y_train)
        save = open("GBM_parameters.pickle", "wb")
        pickle.dump(model, save)

    y_train_predict = model.predict(X_train)
    y_test_predict = model.predict(X_test)

    return y_train_predict, y_test_predict


def LGBM(X_train, y_train, X_test, load_model=False):

    train_dataset = lgb.Dataset(X_train, label=y_train)

    param = {}
    param['metric'] = ['multi_logloss']
    param['objective'] = 'multiclass'
    param['num_class'] = 9

    param['verbose'] = -1
    # params['importance_type'] = 'gain'
    #params['num_iterations'] = 15000
    #params['num_iterations'] = 200
    #params['learning_rate'] = 0.05
    #params['learning_rate'] = 0.001
    #params['boosting_type'] = 'gbdt'
    #params['objective'] = 'regression'
    #params['metric'] = 'mse'
    #params['dart'] = True,
    #params['sub_feature'] = 0.5
    #params['num_leaves'] = 32 #32
    # params['min_data_in_leaf'] = 30
    # params['max_depth'] = 5
    #params['max_bin'] = 128
    # params['bagging_fraction'] = 0.5
    # params['bagging_freq'] = 32

    if load_model:
        open_file = open("LGBM_parameters.pickle", "rb")
        model = pickle.load(open_file)
    else:
        model = lgb.train(param, train_dataset, 100)
        # lgb.plot_importance(model, ax=None, height=0.2, xlim=None, ylim=None, title='Feature importance', xlabel='Feature importance',importance_type='gain', ylabel='Features', max_num_features=None, ignore_zero=True, figsize=None, grid=True, precision=None)
        # plt.show()
        save = open("..\\..\\models\\LGBM_parameters.pickle", "wb")
        pickle.dump(model, save)

    y_train_predict = model.predict(X_train)
    y_test_predict = model.predict(X_test)

    return np.argmax(y_train_predict, axis=1), np.argmax(y_test_predict, axis=1)


def SVM(X_train, y_train, X_test, load_model=False):

    if load_model:
        open_file = open("SVM_parameters.pickle", "rb")
        model = pickle.load(open_file)
    else:
        #model = svm.SVR(C=1e4, kernel='rbf', cache_size=1000) # [SolIrr]
        #model = svm.SVR(C=1e4, kernel='rbf', cache_size=1000) # [SolIrr, Tair]
        model = SVC(C=1e4,
                    kernel='rbf',
                    cache_size=1000)  # [SolIrr, Tair, Zenith, Azimuth]

        model.fit(X_train, y_train)
        save = open("..\\..\\models\\SVM_parameters.pickle", "wb")
        pickle.dump(model, save)

    y_train_predict = model.predict(X_train)
    y_test_predict = model.predict(X_test)

    return y_train_predict, y_test_predict


def DNN(X_train, y_train, X_test, load_model=False):
    if load_model:
        open_file = open("DNN_parameters.pickle", "rb")
        model = pickle.load(open_file)
    else:
        model = MLPClassifier(hidden_layer_sizes=(60, 40, 30,),
                              activation='relu',
                              solver='adam',
                              alpha=1e-5,
                              batch_size='auto',
                              max_iter=int(1e5),
                              n_iter_no_change=50)

        model.fit(X_train, y_train)
        save = open("DNN_parameters.pickle", "wb")
        pickle.dump(model, save)

    y_train_predict = model.predict(X_train)
    y_test_predict = model.predict(X_test)

    print(y_test_predict[0:10])

    return y_train_predict, y_test_predict


def report(results, n_top=3):
    for i in range(1, n_top + 1):
        candidates = np.flatnonzero(results['rank_test_score'] == i)
        for candidate in candidates:
            print("Model with rank: {0}".format(i))
            print("Mean validation score: {0:.3f} (std: {1:.3f})".format(
                  results['mean_test_score'][candidate],
                  results['std_test_score'][candidate]))
            print("Parameters: {0}".format(results['params'][candidate]))
            print("")


def RandomSearch(model, param_dist, n_iter_search, X_train, y_train):

    random_search = RandomizedSearchCV(model, param_distributions = param_dist,
                                       n_iter = n_iter_search, cv = 5, iid = False)
    start = time()

    random_search.fit(X_train, y_train)

    print("RandomizedSearchCV took %.2f seconds for %d candidates"
          " parameter settings." % ((time() - start), n_iter_search))
    report(random_search.cv_results_)


if __name__ == '__main__':
    #############################################################################
    ## Run all models at the same time and add results as columns in new table ##
    #############################################################################
    for i in range(2):
        classes_dict = {0: 'Standing', 1: 'Sitting', 2: 'Walking',
                    3:'Stand-to-walk', 4:'Stand-to-sit', 5: 'Sit-to-stand',
                    6:' Walk-to-stand', 7:'Sit-to-walk', 8: 'Walk-to-sit'}
        classes_list = list(classes_dict.values())

        raw_data_path = '..\\..\\data\\raw\\'
        processed_data_path = '..\\..\\data\\processed\\'

        save_results_path = '..\\..\\reports\\'

        CROSS_VAL = False

        path = processed_data_path + 'all_datasets_combined.csv'

        print("Loading data...")
        df_data = pd.read_csv(path)

        true_labels = df_data['labels'].values.reshape(-1, 1)
        true_labels = true_labels.astype(int)


        #df_data.drop(['labels'], axis=1, inplace=True)
        #data = df_data.values

        #n_samples, n_features = data.shape

        features = []

        features_acc = ['acc_x', 'acc_y', 'acc_z']
        features.extend(features_acc)

        if i == 0:
            features_acc_AC_DC = ['acc_x_AC', 'acc_y_AC', 'acc_z_AC', 
                                'acc_x_DC', 'acc_y_DC', 'acc_z_DC']
            features.extend(features_acc_AC_DC)

        features_gyro = ['gyro_x', 'gyro_y', 'gyro_z']
        features.extend(features_gyro)

        featuers_lacc = ['lacc_x', 'lacc_y', 'lacc_z']
        features.extend(featuers_lacc)

        features_eul = ['eul_x', 'eul_y', 'eul_z']
        features.extend(features_eul)

        pred_col = 'labels'
        print(features)
        X_train, y_train, X_test, y_test = train_test_sets(df_data,
                                                        features,
                                                        pred_col,
                                                        test_size=0.3,
                                                        shuffle=True,
                                                        normalize=False)

        y_train, y_test = np.ravel(y_train), np.ravel(y_test)

        print("X_train shape is: {}".format(X_train.shape))
        print("y_train shape is: {}".format(y_train.shape))
        print("X_test shape is: {}".format(X_test.shape))
        print("y_test shape is: {}".format(y_test.shape))

        n_samples, n_features = X_train.shape
        print("Data of size: ({}, {}) was sucessfuly loaded".format(n_samples, n_features))

        # Loop over all models in list
        # prediction_models = [LGBM, SVM, DNN]
        # column_names = ['LGBM', 'SVM', 'DNN']
        # prediction_models = [SVM, DNN]
        # column_names = ['SVM', 'DNN']
        prediction_models = [DNN]
        column_names = ['DNN']

        # prediction_models = [LGBM, SVM]
        # column_names = ['LGBM', 'SVM']

        for model, column_name in zip(prediction_models, column_names):

            ### Run model ###
            start_time = time()
            y_train_predict, y_test_predict = model(X_train, y_train, X_test, load_model=False)
            end_time = time()

            print("Time taken for training and testing: {0:.4f} s".format(end_time - start_time))
            print("Accuracy {}:".format(column_name))
            print(y_train_predict.shape)
            print(y_test_predict.shape)
            classification_accuracy(y_train, y_train_predict, y_test, y_test_predict)
            print("########################################################\n")

            if CROSS_VAL:
                X_cross = np.concatenate([X_train, X_test], axis=0)
                y_cross = np.concatenate([y_train, y_test], axis=0)
                cross_valid(X_cross, y_cross, model)
            
            

            # # APPENDING NEW RESULTS TO EXISTING TABLE ##
            # y_true = np.hstack((y_train, y_test)).T
            # y_prediction_np = np.hstack((y_train_predict, y_test_predict)).T
            y_prediction_np = y_test_predict
            column_name = 'predicted_label_' + column_name
            y_prediction = pd.DataFrame(data=y_prediction_np).rename(columns = {0: column_name})#, index = data_df.index)
            y_true = pd.DataFrame(data=y_test).rename(columns = {0: 'true_labels'})
            data_df = pd.concat([y_true, y_prediction], axis = 1)

            

        # file_name_save = file_name + "_w_predictions_Gpoa_Tair_Zen_Az.csv"
        data_df.to_csv(save_results_path + str(i) +'_LGBM_predictions.csv')




 
################
# RANDOM SEARCH FOR HYPERPARAMETERS
################

    # param_dist = {'kernel' : ['sigmoid'],
    #               'C' : [0.001, 0.01, 0.1, 0.5],
    #               'gamma' : [1e-4, 1e-3],
    #               'epsilon' : [0.01, 0.1, 1]}
    #
    # n_iter_search = 20
    # RandomSearch(svm.SVR(), param_dist, n_iter_search, X_train, y_train)


################
# GRID SEARCH FOR HYPERPARAMETERS
################

#     tuned_parameters = [{
#                         'kernel' : ['linear', 'rbf', 'sigmoid'],
#                         'C' : [0.1, 1],
#                         'gamma' : [0.1],
#                         #'epsilon' : [1e-2, 1e-3, 1e-4]
#                         }]
#
#
#     # tuned_parameters = [{'kernel' : ['linear'], 'C' : [0.0001, 0.001], 'gamma' : [1e-3], 'epsilon' : [1e-2, 1e-3]}]
#     mean_squared_error_scorer = make_scorer(mean_squared_error)
#     scores = [mean_squared_error_scorer]
#
#     for score in scores:
#         print("# Tuning hyper-parameters for {}".format(score))
#         print()
#
#         reg = GridSearchCV(svm.SVR(), tuned_parameters, cv = 5,
#                             scoring = score) #cv number of fold in cross validation
#         reg.fit(X_train, y_train)
#
#         print("Best parameters set found on development set:")
#         print()
#         print(reg.best_params_)
#         print()
#         print("Grid scores on development set:")
#         print()
#         means = reg.cv_results_['mean_test_score']
#         stds = reg.cv_results_['std_test_score']
#         for mean, std, params in zip(means, stds, reg.cv_results_['params']):
#             print("%0.3f (+/-%0.03f) for %r" % (mean, std * 2, params))
#         print()
#
#         print("Detailed classification report:")
#         print()
#         print("The model is trained on the full development set.")
#         print("The scores are computed on the full evaluation set.")
#         print()
#         #y_true, y_pred = y_test, reg.predict(X_test)
#         #print(classification_report(y_true, y_pred))
#         #print()
