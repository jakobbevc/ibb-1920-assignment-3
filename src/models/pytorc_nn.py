import sys
import matplotlib.pyplot as plt
import numpy as np

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.utils.data as Data

import torchvision.transforms as transforms
import torch.optim as optim

torch.manual_seed(1)

sys.path.append("..")
from utils import utils


class Net(nn.Module):

    def __init__(self, num_features, num_classes):
        super().__init__()

        self.input_size = num_features
        self.output_size = num_classes

        self.hidden1 = nn.Linear(num_features, 60)
        self.hidden2 = nn.Linear(60, 40)
        self.hidden3 = nn.Linear(40, 30)
        self.output = nn.Linear(30, num_classes)

    def forward(self, x):
        x = self.hidden1(x)
        x = F.relu(x)

        x = self.hidden2(x)
        x = F.relu(x)

        x = self.hidden3(x)
        x = F.relu(x)

        x = self.output(x)
        return x


class DatasetNN(Data.Dataset):

    """
        Support class for the loading and batching of sequences of samples

        Args:
            dataset (Tensor): Tensor containing all the samples
            labels (Tensor): Tensor containing all the samples
            sequence_length (int): length of the analyzed sequence by the LSTM
            transforms (object torchvision.transform): Pytorch's transforms used to process the data
    """

    def __init__(self, dataset, labels,  sequence_length=1, transforms=None):
        self.dataset = dataset
        self.labels = labels
        self.seq_length = sequence_length
        self.transforms = transforms

    def __len__(self):
        return self.dataset.__len__()

    def __getitem__(self, idx):
        return self.dataset[idx], self.labels[idx]


def training(model, data_loader, num_epochs):
    model.train()
    running_loss = 0.0
    losses = []
    for epochs in range(num_epochs):
        for i, (data, labels) in enumerate(data_loader):

            output = model(data)

            # remove the 1 dimension for output
            output = output.squeeze()

            # labels = (labels * 10).long()
            labels = labels.view(-1).long()

            loss = loss_function(output, labels)
            losses.append(loss)

            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            # print statistics
            running_loss += loss.item()
            if i % 2000 == 1999:    # print every 2000 mini-batches
                print('[%d, %5d] loss: %.3f' %
                    (epoch + 1, i + 1, running_loss / 2000))
                running_loss = 0.0

    return losses


def predict(dataloader):
    prediction_list = []
    labels_list = []
    correct = 0
    total = 0
    with torch.no_grad():
        for i, (data, labels) in enumerate(dataloader):
            output = model(data)
            _, preds_tensor = torch.max(output, 1)
            total += labels.size(0)
            correct += (preds_tensor == labels).sum().item()

            prediction_list.append(preds_tensor)
            labels_list.append(labels)
    print('Accuracy of the network on the 10000 test images: %d %%' % (100 * correct / total)) 
    return torch.cat(prediction_list, 0), torch.cat(labels_list, 0) 


if __name__ == "__main__":

    NUM_EPOCHS = 1
    NUM_CLASSES = 9

    SEQ_LEN = 0
    BATCH_SIZE = 256

    path_raw = '..\\..\\data\\raw\\'
    path_processed = '..\\..\\data\\processed\\'

    data, true_labels = utils.load_combined_dataset(path_processed)

    unique, counts = np.unique(true_labels, return_counts=True)
    #print(dict(zip(unique, counts)))

    n_samples, n_features = data.shape

    X_train, y_train, X_test, y_test = utils.train_test_split(data,
                                                              true_labels,
                                                              test_size=0.3)

    training_dataset = DatasetNN(X_train, y_train,
                                 SEQ_LEN, transforms=None)

    training_loader = Data.DataLoader(training_dataset, BATCH_SIZE, 
                                      shuffle=True, drop_last=True,
                                      num_workers=0)

    test_dataset = DatasetNN(X_test, y_test,
                             SEQ_LEN, transforms=None)

    # One batch of all he test samples
    test_loader = Data.DataLoader(test_dataset, test_dataset.__len__(),
                                  shuffle=False, drop_last=True,
                                  num_workers=0)

    model = Net(n_features, NUM_CLASSES).double()

    loss_function = nn.CrossEntropyLoss()
    optimizer = optim.Adam(model.parameters(), 
                           lr=0.01, 
                           betas=(0.9, 0.999), 
                           eps=1e-08, 
                           weight_decay=0.1, 
                           amsgrad=False)

    print("Model summary: {}".format(model))

    losses = training(model, training_loader, NUM_EPOCHS)

    y_train_predict, y_train = predict(training_loader)
    y_test_predict, y_test = predict(test_loader)

    utils.classification_accuracy(y_train, y_train_predict,
                                  y_test, y_test_predict)

    plt.plot(losses)
    plt.show()
