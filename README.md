Human action recognition 
==============================

Human action recognition by using accelerometer data.

To run the LightGBM, SVM or DNN, run 'python models.py'. Before that edit the code, selecting models and features to run.

Project Organization
------------
    ├── README.md          <- The top-level README for developers using this project.
    ├── **data**
    │   ├── **processed**  <- The final, canonical data sets for modeling.
    │   └── **raw**       <- The original, immutable data dump.
    │
    │
    ├── notebooks          <- Jupyter notebooks. Naming convention is a number (for ordering),
    │                         the creator's initials, and a short `-` delimited description, e.g.
    │                         `1.0-jqp-initial-data-exploration`.
    │
    ├── reports            <- Generated analysis as HTML, PDF, LaTeX, etc.
    │   └── figures        <- Generated graphics and figures to be used in reporting
    │
    ├── requirements.txt   <- The requirements file for reproducing the analysis environment, e.g.
    │                         generated with `pip freeze > requirements.txt`
    │
    ├── setup.py           <- makes project pip installable (pip install -e .) so src can be imported
    ├── src                <- Source code for use in this project.
    │   ├── __init__.py    <- Makes src a Python module
    │   │
    │   ├── **data**       <- Scripts to download or generate data
    │   │   └── process_data.py
    │   │
    │   ├── **models**        <- Scripts to train models and then use trained models to make
    │   │   │                 predictions
    │   │   ├── models.py
    |   |   |-- pytorch_nn.py
    │   │   └── pytorch_LSTM.py
--------

