# """
# =========================================
# Feature importances with forests of trees
# =========================================

# """
# print(__doc__)
import sys

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from sklearn.datasets import make_classification
from sklearn.ensemble import RandomForestRegressor


sys.path.append("..")
from data.process_data import train_test_sets


raw_data_path = '..\\..\\data\\raw\\'
processed_data_path = '..\\..\\data\\processed\\'

CROSS_VAL = False

path = processed_data_path + 'all_datasets_combined.csv'

print("Loading data...")
df_data = pd.read_csv(path)

true_labels = df_data['labels'].values.reshape(-1, 1)
true_labels = true_labels.astype(int)

features = []

# features_acc = ['acc_x', 'acc_y', 'acc_z']
# features.extend(features_acc)

# features_acc_AC_DC = ['acc_x_AC', 'acc_y_AC', 'acc_z_AC', 
#                       'acc_x_DC', 'acc_y_DC', 'acc_z_DC']
# features.extend(features_acc_AC_DC)

# features_gyro = ['gyro_x', 'gyro_y', 'gyro_z']
# features.extend(features_gyro)

# featuers_lacc = ['lacc_x', 'lacc_y', 'lacc_z']
# features.extend(featuers_lacc)

features_eul = ['eul_x', 'eul_y', 'eul_z']
features.extend(features_eul)

pred_col = 'labels'

X_train, y_train, X_test, y_test = train_test_sets(df_data,
                                                   features,
                                                   pred_col,
                                                   test_size=0.3,
                                                   normalize=False)
y_train, y_test = np.ravel(y_train), np.ravel(y_test)
# X_train_norm, X_test_norm = normalize_data(X_train, X_test)

# Build a classification task using 3 informative features
X, y = X_train, y_train

# Build a forest and compute the feature importances
forest = RandomForestRegressor(n_estimators=250,
                               random_state=0)

forest.fit(X, y)
importances = forest.feature_importances_
std = np.std([tree.feature_importances_ for tree in forest.estimators_],
             axis=0)
indices = np.argsort(importances)[::-1]

# Print the feature ranking
print("Feature ranking:")

for f in range(X.shape[1]):
    print("%d. feature %d (%f)" % (f + 1, indices[f], importances[indices[f]]))

# Plot the feature importances of the forest
plt.figure()
plt.title("Feature importances")
plt.bar(range(X.shape[1]), importances[indices],
       color="b", yerr=std[indices], align="center")
plt.xticks(range(X.shape[1]), indices)
plt.xlim([-1, X.shape[1]])
plt.show()
