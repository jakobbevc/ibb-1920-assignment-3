import os
import sys

import matplotlib.pyplot as plt

import torch
import torch.utils.data as Data

import torchvision.transforms as transforms
import torch.optim as optim

import torch.nn as nn
import torch.nn.functional as F

torch.manual_seed(1)

sys.path.append("..")
from utils import utils


class LSTMTagger(nn.Module):

    def __init__(self, num_features, hidden_dim, num_layers, num_classes, batch_size):
        
        super(LSTMTagger, self).__init__()
        
        self.hidden_dim = hidden_dim
        self.num_features = num_features
        self.batch_size = batch_size
        
        # The LSTM takes word embeddings as inputs, and outputs hidden states
        # with dimensionality hidden_dim.
        self.lstm = nn.LSTM(input_size=num_features,
                            hidden_size=hidden_dim,
                            num_layers=num_layers,
                            batch_first=True).double() #check

        # The linear layer that maps from hidden state space to tag space
        self.hidden2tag = nn.Linear(hidden_dim, num_classes)
        # tagset_size = number of classes

    def forward(self, data):
        #print(data.shape)
        lstm_out, _ = self.lstm(data.double())
        # We want to take only the laste prediction
        # Similarly than keras return sequence = False
        lstm_out = lstm_out[:, -1].float()
        tag_scores = self.hidden2tag(lstm_out.view(-1, self.hidden_dim))
        
#         tag_space = self.hidden2tag(lstm_out.view(-1, self.hidden_dim))
#         tag_scores = F.log_softmax(tag_space, dim=1)

        return tag_scores, len(lstm_out)


class DemoDatasetLSTM(Data.Dataset):

    """
        Support class for the loading and batching of sequences of samples

        Args:
            dataset (Tensor): Tensor containing all the samples
            sequence_length (int): length of the analyzed sequence by the LSTM
            transforms (object torchvision.transform): Pytorch's transforms used to process the data
    """

    ##  Constructor
    def __init__(self, dataset, labels,  sequence_length=1, transforms=None):
        self.dataset = dataset
        self.labels = labels
        self.seq_length = sequence_length
        self.transforms = transforms

    ##  Override total dataset's length getter
    def __len__(self):
        #return self.dataset.__len__()
        return self.dataset.__len__() - self.seq_length + 1

    ##  Override single items' getter
    def __getitem__(self, idx):
        
        # if self.seq_length is not None:
        #     return self.dataset[idx:idx+self.seq_length], self.labels[idx:idx+self.seq_length]
        # else:
        #     return self.dataset[idx], self.labels[idx]
        if self.seq_length == 0:

            return self.dataset[idx], self.labels[idx]

        else:
            
            return self.dataset[idx:idx+self.seq_length], self.labels[idx:idx+self.seq_length]
        
        # if idx + self.seq_length > self.__len__():
        #     if self.transforms is not None:
        #         item = torch.zeros(self.seq_length, self.dataset[0].__len__())
        #         item[:self.__len__()-idx] = self.transforms(self.dataset[idx:])
        #         return item, item
        #     else:
        #         item = []
        #         lab = []
        #         item[:self.__len__()-idx] = self.dataset[idx:]
        #         #lab[:self.__len__()-idx] = self.labels[idx:]
        #         return item, item
        #         #return self.dataset[self.__len__() - self.seq_length:-1], self.labels[self.__len__() - self.seq_length:-1]
        # else:
        #     if self.transforms is not None:
        #         return self.transforms(self.dataset[idx:idx+self.seq_length]), self.transforms(self.dataset[idx:idx+self.seq_length])
        #     else:
        #         #print(idx)
        #         return self.dataset[idx:idx+self.seq_length], self.labels[idx:idx+self.seq_length]


def train(data_loader, num_epochs):
    losses = []
    for epoch in range(num_epochs):
        for data, labels in data_loader:
            
            optimizer.zero_grad()
            
            # Step 3. Run our forward pass.
            tag_scores, labels_size = model(data)
        
            # Labels have to be one dimesional, can check details 
            # in documentation for CrossEntropyLoss, additionaly
            # class index is expected to be in the range [0, C-1]
            # where C is nubmer of classes
            labels = labels[:, -1].view(labels_size).long()
            #labels = (labels * 10).long()

            
            # Step 4. Compute the loss, gradients, and update the parameters by
            #  calling optimizer.step()

            loss = loss_function(tag_scores, labels)
            losses.append(loss)
            loss.backward()
            optimizer.step()
 
    return losses


def predict(dataloader):
    prediction_list = []
    labels_list = []
    correct = 0
    total = 0
    with torch.no_grad():
        for i, (data, labels) in enumerate(dataloader):
            output, labels_size = model(data)
            # print(output)

            labels = labels[:, -1].view(labels_size).long()
            _, preds_tensor = torch.max(output, 1)

            # print(preds_tensor.shape)
            # print(labels.shape)
            total += labels.size(0)
            correct += (preds_tensor == labels).sum().item()

            prediction_list.append(preds_tensor)
            labels_list.append(labels)
    print('Accuracy of the network on the 10000 test images: %d %%' % (100 * correct / total)) 
    return torch.cat(prediction_list, 0), torch.cat(labels_list, 0) 


if __name__ == '__main__':
    NUM_EPOCHS = 10
    NUM_CLASSES = 9

    SEQ_LEN = 20
    BATCH_SIZE = 128

    HIDDEN_SIZE = 32
    NUM_LAYERS = 2

    path_raw = '..\\..\\data\\raw\\'
    path_processed = '..\\..\\data\\processed\\'

    data, true_labels = utils.load_combined_dataset(path_processed)
    n_samples, n_features = data.shape

    X_train, y_train, X_test, y_test = utils.train_test_split(data, 
                                                              true_labels,
                                                              test_size=0.3)

    training_dataset = DemoDatasetLSTM(X_train, y_train, 
                                       SEQ_LEN, transforms=None)

    training_loader = Data.DataLoader(training_dataset, BATCH_SIZE,
                                      shuffle=False, drop_last=True,
                                      num_workers=0)

    test_dataset = DemoDatasetLSTM(X_test, y_test, 
                                   SEQ_LEN, transforms=None)

    test_loader = Data.DataLoader(test_dataset, BATCH_SIZE,
                                  shuffle=False, drop_last=True,
                                  num_workers=0)

    model = LSTMTagger(n_features, HIDDEN_SIZE,
                       NUM_LAYERS, NUM_CLASSES,
                       BATCH_SIZE)

    loss_function = nn.CrossEntropyLoss()
    optimizer = optim.SGD(model.parameters(), lr=0.01)

    losses = train(training_loader, NUM_EPOCHS)

    y_train_predict, y_train = predict(training_loader)
    y_test_predict, y_test = predict(test_loader)

    utils.classification_accuracy(y_train, y_train_predict,
                                  y_test, y_test_predict)

    plt.plot(losses)
    plt.show()
